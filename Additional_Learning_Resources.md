# DISCLAIMER
** The following list of resources for learning are based on my own personal experiences and are in no way endorsed by the professor of this class. 
I'm definitely not qualified since I don't have a lot of time since I started learning Python for Deep Learning so take everything in here with a grain of salt. Feel free to add your own resources and materials which could be useful for others.** 

# Markdown (.md)
* [Guide to Markdown syntax](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet): Markdown is the language/syntax that allows to format documents in this website and others that use GIT. This one is more condensed compared to the one listed in the README_DIARY.md file.

# Python
* [Book: Automate the boring stuff with Python by Al Sweigart](http://automatetheboringstuff.com/): Chapter 1 to 6 should be enough for this class and for most purposes (basically, what we covered in the introduction class to Python but in much more detail), although the rest of the chapters cover a variety of useful topics which depending on your interests I would still recommend.
* [Book: Learn Python The Hard Way by Zed Shaw](https://learnpythonthehardway.org/python3/): have never read it but the professor and many people suggest it as a good starting point for Python.
* [Introduction to Python 3 Programming Youtube playlist by sentdex](https://www.youtube.com/watch?v=eXBD2bB9-RA&list=PLQVvvaa0QuDeAams7fkdcwOGBpGdHpXln): if you prefer videos rather than reading. sentdex is one of the most successful online educators for Python programming and he has videos which cover a lot of aspects of the language.
* [Youtube Playlist on Object Oriented Programming (OOP) in Python by Corey Schafer](https://www.youtube.com/watch?v=ZDa-Z5JzLYM&list=PL-osiE80TeTsqhIuOqKhwlXsIBIdSeYtc): OOP is a programming paradigm which focuses on objects. The main concepts to grasp are the idea of classes (and inheritance, which we covered in class), attributes and methods which are used extensively through Python but I would not recommend this for beginners since it can be a bit abstract.

# NumPy
* [Python and NumPy written tutorial by Stanford for CS231n](http://cs231n.github.io/python-numpy-tutorial/#python): brief written tutorial on the basic syntax of Python and NumPy, one of the main numerical linear algebra libraries in Python. 

# pandas
* [Introduction to Data Science in Python by University of Michigan, Coursera](https://www.coursera.org/learn/python-data-analysis): this one can serve as a great introduction to the things you can do in pandas, one of the main libraries for data manipulation and analysis. You don't need to pay to join the course, just click audit and can listen to the lectures.

# Jupyter Notebooks and Google Colab
* [Getting Started With Jupyter Notebook for Python](https://medium.com/codingthesmartway-com-blog/getting-started-with-jupyter-notebook-for-python-4e7082bd5d46): blog article which describes Jupyter Notebooks, a pretty useful tool for quick and interactive deployment of code. 
* [Introduction to Google Colaboratory](https://colab.research.google.com/notebooks/welcome.ipynb): Google Colab is basically a Jupyter Notebook stored in Google Drive which makes it so you can run code without installing Python on your computer.

# Machine Learning
* [Machine Learning by Andrew Ng, Coursera](https://www.coursera.org/learn/machine-learning): one of the most recommended online resource to learn about (traditional) machine learning. Covers supervised learning: linear regression, logistic regression, (traditional) neural networks, support vector machines (SVMs), and unsupervised learning: K-means clustering and Principal Component Analysis (PCA), among a few other topics related to ML projects.

# Deep Learning
* [Deep Learning Specialization by Andrew Ng, Coursera](https://www.coursera.org/specializations/deep-learning): 5 courses covering different aspects of DL. The first two are focused on (traditional) (Artificial) Neural Networks, the 3rd one covers some issues with ML projects, 4th one covers Convolutional Neural Networks (CNNs) and the 5th one covers Recurrent Neural Networks (RNNs). Can audit (for free), no need to pay.

P.S.: If anyone is interested in study groups for Python/Data Science/Machine Learning/Deep Learning send me an email to let me know and if there's enough people maybe we can organize something (e24047028@mail.ncku.edu.tw).