This diary file is written by Edwin Arkel Rios E24047028 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.
# 2019-02-20 #
* The introductory lecture highlighted some pretty interesting successes of Artificial Intelligence.

# 2019-02-27 #
* The class of today was great, but we definitely required more time to cover all of the topics originally scheduled for today.
I particularly was excited for hearing about the stories of "AI Success" of others.
* Something that was confirmed for me today is the fact that Data Science, Data Mining, Machine Learning, 
Deep Learning, Artificial Intelligence and even Statistics are buzzwords and as so should be interpreted with caution depending on the context.

# 2019-03-06 #
* The functionality of GIT explained today including version control and collaborative explain why it is used so frequently among programming groups.
* Why are they teaching classes and inheritance in a half an hour introduction to Python? OOP is important but in a such a 
short period of timeI find it unnecessary and confusing for newcomers.
* Like last week, we didn't finish. Time is definitely a challenge in this class.
* NumPy and modules definitely require a single clas due to their sheer importance in this field.

# 2019-03-13 #
* This lecture was pretty fun. Even though the exercises were simple I think going through the Jupyter Notebook ourselves made the concepts a lot more clear
compared to just listening to a lecture like last week.

# 2019-03-20 #
* Why compare AI learning with human learning?
* The reinforcement learning part was confusing. I don't understand why do we need infinite amount of data.

# 2019-03-27 #
* There's so many different kinds of neural network architectures. Do professionals in this field actually understand all of them?
How to know which one to use?
* The professor keeps mentioning that training a NN is like art. There's so many hyperparameters to choose from. 
There being no exact reasoning on how to choose them and there being so much trial-and-error, does this still count as "science"?

# 2019-04-10 #
* Are the "filters" in convolutional layers similar to other filters used in signal processing such as Low, High Pass or Moving Average?

# 2019-04-17 #
* TensorFlow 2.0 makes it seem way easy to train models, but I would think there's a catch and it's not actually that simple to do useful things.
* Are there models in AI which don't require big amounts of data to produce good results?
* Is it actual democratization of DL if to do it you still require big amounts of data which only the giants of IT such as Google have?
Isn't it in this case only a bait to make you interested in something you cannot get as good good results by yourself so you rely on their services?

# 2019-04-24 #
* I find it interesting that apparently you can run TF2.0 style code in TF1.13 but no the other way around.
* TF2.0 and TF1.13 actually don't have as many differences as I originally thought but there's some QoL improvements,
such as a live update along with an ETA display for the training process.

# 2019-05-01 #
* I honestly would prefer it if students  who don't finish during the class would go home and finish it as homework. 
I find it a little bit dissapointing that we're going to cut short or not cover at all some of the other sections to handhold 
people through assignments that were honestly not complicated at all.

# 2019-05-08 #
* I liked the lecture today. It was great that the professor spent some time clarifying questions students had.
* The analogy to the phone was great. The only thing I would change would be to use something along the lines of: "Is using Deep Learning easy", 
instead of "Is Deep Learning easy?"

# 2019-05-15 #
* Lecture this week was probably one of the best in the whole semester. I personally think discussing non-technical stuff about "AI" for a 
1 hour-course such as this is a better use of class time. It's both interesting and probably feels more relatable to more students than the math 
behind backpropagation.
* That doesn't make the math part less important and I think it would be great if the professor would open
a more formal 3 credits-course that goes in depth into the math of deep learning.

# 2019-05-22 #
* I am familiar with the Algorithm Bias issue with AI, but the solution seems really messy. The problem of dealing with an imbalanced dataset of 
faces can be solved by getting more information to make the dataset more neutral, but what about Bias related to let's say
results from previous loans, jobs, criminal records, things that even humans still stereotype about. How can we make algorithms to avoid
the Bias we ourselves cannot?
* I'm excited to begin working on the final project. Hope my groupmates are as interested so we can obtain great results.
* As for the contents of the lecture itself, while I don't agree with many of the expectations of AI taking over a majority of jobs, 
I still find it interesting and a great topic for discussion.

# 2019-05-29 #
* I didn't come to class during this week.

# 2019-06-05 #
* Our first meeting. Even if most of our group members had dropped the class, the ones left were pretty good teammates and I enjoyed discussing with them.

# 2019-06-12 #
* Some of the presentations were pretty creative and a great inspiration for our own.

# 2019-06-19 #
* The final exam was pretty reasonable. Indeed some questions were based on "memory" but honestly if you paid attention during 
the semester even the questions related with time could be deducted from logic and understanding of the material.