This diary file is written by David Huang E64066240 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #

* The first lecture really surprised me.
* It never comes to my mind that AI can be used in so many different spheres, like medical area and lip recognition.
* With AI, we can solve problems that are hard to solve or even implement by coding, for example, speech recognition.
* AI will definitely have critical impacts on the development of entire world.


# 2019-02-27 #

* Today, I learned many definitions of AI, ML, DL, Data Science and so on.
* I thought that today's lecture was a great introduction.
* With many different definitions, I can interpret these techonologies in different way and think from different angles.

# 2019-03-06 #

* Today's lecture was about the tutorial of Python.
* The examples in the slide are quite clear, which I think is suitable for Python beginner or even programing beginner.
* I have a question in page 24 in the slide. Can "Array.\_\_init\_\_(self, entry, rows, columns)" be moved?
* Why I ask this question is that in other language, sometimes we don't always know the class we inherit from. Then how do we know the \_\_init\_\_ frunction?
* It seems have same result if I comment "Array.\_\_init\_\_(self, entry, rows, columns)".

# 2019-03-13 #

* Today, I reviewed some basic part of Python and finished some exercise.
* Actually, I am rather interested in the rest of the exercises -- MNIST with Keras and Tensorflow.
* Because Keras and Tensorflow are tools that I didn't practice very much before.

# 2019-03-20 #

* Today's lecture is an introduction to Neural Network.
* I learned how CNN works, however, I am more interesting to learning how to program CNN by using toolkits like Tensorflow.
* Next week's lecture will introduce backpropagation, a concept I think more complicated than other ideas or topics in the lecture.
* I hope that I can have a clear understanding of next week's lecture.

# 2019-03-27 #

* Today's lecture is about how to find parameters for construsting a useful network.
* I consent professor's argument that every model is wrong, because if a model can approximate functions with no error, that should be a system, not a model.

# 2019-04-10 #

* Today's lecture was talking about what factors would influence the gradient decent and that the concept of CNNs, including the filter, pooling and padding.
* After the introduction of the theories, I am looking forward to the Tensorflow practice in the next week.

# 2019-04-17 #

* Today's lecture was about how to train models by using Keras and Tensorflow and how to depoly the trained model to the internet by using Flask.
* Deploying the trained model is quite convenient for others to use our model, also we can improve our model via the data others upload to the website.

# 2019-04-24 #

* Today's class I did some practices about Tensorflow.
* I was disappointed that we didn't teach Docker today.
* Hope that Docker will be introduced next week.

# 2019-05-01 #

* Today's class was still about practicing Tensorflow.
* Look forward to the Flask practice next week.

# 2019-05-08 #

* In today's lecture, teacher helped us review some concepts we have learned so far and answered some questions that made us confuse.
* Teacher's answering questions let me know that AI have benchmark just like GPU.
* Via benchmark, I can know what to do first when I want to start training a new task.

# 2019-05-15 #

* Today's lecture was about automation and the future of work.
* Via today's class, I know what industrial evolutions are about and a prediction that many jobs will disappear in the future due to the automation.
* It influence me to some extent that perhaps fields accosiated with automation will be considered in my career.

# 2019-05-22 #

* In today's lecture, we discuessed algorithm bias and the consequences may have if we harness AI in an inappropriate way.
* Group project is challenging to me. I hope that our group can achieve a high accuracy.

# 2019-05-29 #

* Today, our group discussed the group project in the lecture.
* We want to use a model which consists of 2 hidden layers, each layer with dropout and 1600 nodes.
* As for other configuration, loss function will be cross entropy, Relu and Adam will be applied at the first time.
* Hope that we can train a model that have a better performance than that in the class.

# 2019-06-05 #

* Today, we still discussed how to make our model better.
* So far, the best structure of our model is dense-dense-dropconnect layer with 128, 64, 64 nodes respectively.
* The possibility in the dropconnect is 0.5, and the batch size is 512.
* The best accuracy we can have now is 97.56%.
* Next step, we are trying to add another dropconnect layer to see whether our model can have better performance or not.

# 2019-06-12 #

* Today is first group presentation in this course. Four groups presented their results of mnist digit recognition to class members.
* I learned a lot in today's group presentation.
* So many models and materials other classmates used I have never heard about, such as inception model.
* So far, we only have an accuracy of 98.5% or so; however, groups presented today all have an accuracy more than 99%.
* I noticed that groups presented today all use CNN in their models.
* Hope that we can train our model as well as other groups' without using CNN.

# 2019-06-19 #

* Today is our group presentation.
* I think I was too nervous during the presentation.
* Got 25 of 30 in the exam. Not very good but acceptable.
* This course is so informative. Hope that I have chance to attend courses pretaining to AI like this course.