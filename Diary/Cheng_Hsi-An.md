This diary file is written by Shawn Cheng H14061080 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

#2019-02-21
* It amazes me how fast AI is improving, and how accurate it is.
* I never thought about applying AI to lip reading and medicine.

#2019-02-28
* I learned about the possible definitions of AI, deep learning, data mining, etc., and how different fields are connected to one another.
* I learned that as fields evolve, the definitions and meaning of the field also changes.
* I also think that professor Martins teaching style differs a lot from my other Taiwanese professors; he puts more emphasis on  student participation in class.

#2019-03-06
* I learned the basics of python syntax.
* I think the pages regarding variables and function, mainly the array examples were hard to follow.

#2019-03-13
* Today I learned more about python syntax.
* I also learned how to use visual studios to run code.
* I am very grateful to the teaching assistants, they were very helpful and kind.

#2019-03-20
* Today's lecture was great, I learned a lot about Neural Networking.
* The lecture was a bit hard to understand though, I will probably review it this weekend.
* I knew about reinforcement learning before, however supervised and unsupervised learning is new to me.
* I found it fascinating that through different layers and nodes, we can create a neural network that can read handwritten numbers.

#2019-03-27
* Today we learned about capacity and how parameters can be both a strength and a weakness.
* We also dived into a philosophical question: Why are all models wrong?
* I also now understand the mathematical function representing the neural network and it's underlying layers, nodes and weights.
* Personally I found today's class a lot easier to follow and understand.

#2019-04-10
* Today we learned about convolutional networks used in image recognition.
* I found the concept of a "convolutional layer" to slide over data very interesting.
* However I also found this week's lesson harder to follow.
* I'll make sure to write my diary on time next week.

#2019-04-17
* Today we learned about tensor flow and its applications.
* We also learned the steps on how to create a deep learning program in python using tensor flow.
* I'm nervous about next week when we have to implement what we learned.

#2019-04-24
* Today we learned about how to implement tensorflow using python.
* The concepts are rather vague and I found it hard to follow in class.

#2019-05-01
* Today we went over tensorflow again but in more detail.
* I found todays course to be extremely helpful, I feel that I have a better understanding of the code, going over the exercises together helped greatly.
* I still think deep learning is hard, however now I understand it a bit better.
* However I am still worried about the group project.

#2019-05-08
* Today we reviewed what we learned in our previous lessons.
* I understand the big picture of deep learning and how it functions overall, but I still don't understand the mathmatical and coding behind it.
* In short I think I can explain deep learning, backpropagation etc, however I cannot actualy write or change code for a neural network.

#2019-05-15
* Today we learned about the third industrial revolution.
* We also learned about automation and what it can mean for our future.
* After todays class, I'm afraid that accounting will be automated. However I also heard that AI will not remove accounting as a career but will lighten the 
  workload of accountants(AI can focus on repetitive tasks), thus giving accountants more time to handle more advanced problems(auditing, cost analysis).
  
#2019-05-22
* Today we learned about algorithm bias with examples ranging from feeding AI false data to having software to create fake videos of celebrities and politicians.

#2019-05-27
* Today we were assigned to groups of 4 to work on our final project.
* I think my teammates are nice people, but none of us has any prior coding experience.
* I was selected as the group leader even though I don't want the job.

#2019-06-05
* Today we worked on the code.
* We tried using a convolutional neural network but the accuracy decreased.

#2019-06-12
* Today we listened to group 1-4 presentation.
* I think each group did a good job.
<<<<<<< HEAD
* I feel bad for the international students who can't find courses in english.
=======
* I feel bad for the international students who can't find courses in english.
>>>>>>> origin/Shawnzion/cheng_hsianmd-edited-online-with-bitbuck-1560352809859
