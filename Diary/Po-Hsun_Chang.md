This diary file is written by Po-Hsun Chang E24086056 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-19 #

* I knew the three applications of AI already.
* I learnt the reasons why AI is hard to be defined.
* I’ve never thought that asking a robot to pick up an item requires so much work.
* The success story of AI which indicates the AI dressing mirror surprised me.
* Applying AI to save wildlife did inspired me.
* I hope that the course material could be released on time on Wednesday afternoon.