

This is the diary of Meghi Chen C60475360 for Introduction to AI and deep learning for Engineers and Scientists

#2019-02-27#
1. lecture of introduction to every field of these AIs have made me know more about data science, data mining , machine learning,deep learning,system identification
2. i also learned much about the domain knowledges since I am a biotechnology student , it will much relates with my major
3. i wish to know more about how the AI works, like i have read many articles about AI and deep learning .. it always mentions about algorithms. why ai need to conclude this algorithms

#2019-03-06#
1. glad to know more about phyton since I am curious about the coding things
2. wondering about the mathematics using phyton , why people use phyton to do the maths coding.. also why phyton is needed for ai and deep learning
3. thank you for the today's lecture , i think it was interesting and easy to understand


#2019-03-13#
1. havent learned about python or any coding before
2. at first it was kinda confusing but successfully managed it
3. today's class was so fun and thanks for the explanation on website , its really useful
4. i havent tried all of the coding formula but so far so good and enjoy it.

#2019-03-20#
1. I feel today�s lecture was really great , this neural learning made me understand about what i was curious before like the algorithm and how its being applied , not only just that but the basic machine learning is good !
2. i know about this classification that seeing by icon and regression by numbers.
3. Training and testing are needed in order here and i have learned so much about training data and use it to make predictions as close as possible to get the best measurement and test the model
4. also about the cake theory i think its really interesting and easier for me to understand what professor wanna talk about (reinforcement , unlabelized , labelized 

#2019-03-27#
1. today’s lecture started with continuation of what we have learned last week 
2. Machine learning basics : it provides samples and features samples example images, patient or piece of text and the features contain of variables that we want.
3. Classification and Regression predictive performance in classification , it uses true and false accompanied with positive & negative with measuring the accuracy and true/false rate.in regression  -> measured by the amount of error and using some kind of math formula ( i thought it is kind of complicated but cool
4. we learned about the capacity and hypothesis . Capacity where we set our parameters and we can see like our creation has what weakness and strength while this hypothesis like how we assume them to make ( using ML algorithm ) and prove it . High capacity = big hypothesis 
5. big data proved we have a success neural network since it indicates we have a good measurement of algorithms , trials and other preparations. 
6. Bias and Variance ; two different sources of error ( an estimator ) . Bias measures the math of error and variance is more likely measure of what estimator value of sampling and why is likely cause. to have the best machine learning we aimed for low bias and variance . i think its hard to be understood and i don’t really know whether my explanation above are correct.
7. under / over fitted classification and regression told about low / high bias variance it has.
8. usually in underfitting it has high of training and validation but while the overfitting has a low training error but high validation error so we need to make it in between them to reach both of the errors are low.
9. professor asked us to discuss about why all the models are wrong because how many measurement we gave we can’t complete them everything and we can’t distinguish them from true system.
10. machine learning purpose isn’t to find one single best algorithm but the task find  algorithm that work the best.
11. Neural networks consist of input and output layer with hidden layer in the middle . it consists of many types with i think has such a cool structure .
12. also we learned about the gradient and backpropagation 
13. back propagation of how it is calculated ( choose our weight and minimize our loss) -> get a better prediction. after training the network , test with a different one . in the video , it assumed the average cost of measurement 

#2019-04-10#
1. professor started lecture with SGD ( Stochastic Gradient Descent)-> core of algorithm in deep learning
2. initial our network to calculate the potentials based on data and try the trial and error.
3. he asked us to discussion about this SGD , i myself kind of hard to think about this as my basic knowledges only know this is a math calculation that can determine this deep learning using gradient something but still can’t find out how and why the pattern can form like the picture shown in PPT.
4. designing the network at first is an art but to make it possible and calculate the potentials, it needs science.
5. As an engineer , the global optimum is not the main goal to be reached instead to have parameter choices successful to work.Global optimum is not the goal since the model is overfitted and if there is an optimum then what to do with the noises that we have already inputted.
6. Learning rate : if we have it too large , the calculations of gradients lead to mess up and complicated . it end up in random locations and miss the global minima ( long step is taken)
7. too small : getting stuck in local minima and takes long time in training
8. and how do we fit the balance in learning rate?
9. Convolutional Neural networks usually used in image classification
10. divided into many kinds of layers : deep layers, convolution layers
11. convolution layers : we design and we filter ( contain weight and bias -> mathematical structure)-> detect a local feature and in order to use it provide many filters ( feature maps) 
12. every filter is different in features . professor said the large data set have the better filter.
13. this convolution neural network established in 2012 and outperform others in image classification , it is inspired by human eyes
14. pooling layers are applied in CNN -> max pooling ( the adjusted and right position) and average pooling is where it has mismatched whether a slight / medium level of mismatching. 
15. how large “steps” taken in filtering like they jump from one into another , calculating the stride.
16. padding : preserve the size of output maps
17. data augmentation : generate more examples from one image , neural network generalized better + increasing amount of data ( rotate it ,etc)

#2019-04-17#
1. tensor : generalization of vectors and matrices to have a higher dimension 
2. tensor flow : open source software library for building and deploying machine learning models ( we will  practice this next week )
3. Tensor flow is collaborate with Keras and MNIST
4. database of handwritten digits : have the training and test set
5. digits are normalized and have a fixed sized image
6. teacher taught us about loading MNIST data, preprocessing and building the model using some kind of code data we need to input which I thought kind of complicated but made me curious to know
7. editing the model with layers ( last layer : prediction of number and there is Dense(10, activation=‘softmax’) 10 is for the digits used , softmax = to maximize the probability )
8. third layer -> Dense(512, activaton=’signoid’) -> hidden layers  to give a feature of recognizing digits
9. Sequential(I first layer
10. Flatten(input_shape=(28,28.1),
11. and after optimize all of the data -> train it
12. first input , load data , define our network , define our solver ,design training ,  train them， and save the model ( actually for the basic knowledge its acceptable but for applying i think its kind of complicated)
13. teacher asked us to discuss about this tensor flow ? is it can only recognize number digits
14. and yes we discussed about changing the input of package to recognize other
15. turned up it can’t since it is artificial neuro-intelligence ( same model to other task) but only can be made in numbers and researchers are trying to make this modified but still until know , it haven’t worked.
16. if we want to made the model to another thing , we can retrain it and we can make it to do many kinds of training
17. Tensor flow is made to do all of things they trained and its based on our data.
18. teacher taught us on how we model it as a web using python and i think its quite simple ! 

#2019-04-24#
1. today was so fun ! because we practiced the tensor flow
2. but i spent like half of the course to download it since my pip is pip3 and we just realized it
3. after that trying to input the codes in sublime text and test it in my terminal
4. it didnt go that smoothly , we need to download many things 
5. i have learned something really useful today about the pip
6. so the pip is for we download anything in terminal
7. and i found it quiet efficient and practical 
8. i am really interested to learn it more about the tensor flow , python
9. today's practice got me a lot of cool knowledges

<<<<<<< HEAD

#2019-05-01#
1. so first I thought that result of this tensor flow in terminal will be like a picture
=======
#2019-05-01#
 1. so first I thought that result of this tensor flow in terminal will be like a picture
>>>>>>> origin/meghiya/meghi_chenmd-edited-online-with-bitbucke-1556688924454
2. but its like a loading with showing your accuracy and loss
3. then since i have succesed in first one , i tried the exercise given
4. first assignment we need to have 2 layers then we need add more layer , by copy paste the code of Dense(512, activation='sigmoid'),
5. then we need to change hidden layers to relu
6. so as I found that the different is as I added more layer it loss and accuracy is smaller than the first one 
7. adam and relu is a better compilation compare to the sigmoid and sgd
8. first ,we use the sgd to optimizer  and the third exercise want us to change it into adam
9. what i found is when I have added layer then it took longer time to load because it needs to filter more than before.
10. I don’t think its not either hard or easy , its medium level as long as we want to try and learn about it
11. we need to know which one is the best optimizer and compilation -> to have the highest accuracy and smallest loss 
12. to conclude , i think i  kind of know how to use this tensor flow and this introduction class awesome
<<<<<<< HEAD
13. but i think the hard thing that i still experienced is about my tensor flow knowledge like which one is better , what its function 
14. and how this code works that i think i haven’t understood it clearly but yes I’m now have a basic knowledge about it 

#2019-05-08#
1. today we had a review lecture about what professor had taught us in his previews lectures 
2. why we have artificial neural network? -> attempt to simulate the network of neurons that make up a human brain so that the computer will be able to learn things and make decisions in a humanlike manner
3. it's based on the neuron of brain ( input , integrate , output) 
4. convolution layers -> use the filter of slides ( building layers ) for classification image 
5. define the filter and slide down into another layer and do this until the full picture assigned
6. how many filters that we should have? what kind of layer we will insert?
7. back propagation of how it is calculated ( choose our weight and minimize our loss) -> get a better prediction. after training the network , test with a different one . in the video , it assumed the average cost of measurement 
8. back propagation ( update our weight , minimize our function) -> backward testing to measure all of the possibilities that can happen
9. how back propagation works -> it needs to calculate using partial derivatives  (minimize the objectives)-> calculate the cost function regard to the weight
10. back propagation allows the network has hidden layers ( the probabilities that creator doesn’t expect it can happen) ex classification of dog turned out to cat.
11. all the partial derivatives related to a(L) -> LAYER?
12. watch back propagation calculus / deep learning
13. in my opinion , yes this deep learning is hard, its because our limited time also that we can’t expand it more but i feel like since its an introductory class , it led me to learn many things about ai , deep learning and simple coding . i feel its really helpful.
14. apply neural network to identify numbers
15. in tensor flow , to get a prediction , we need to input the model. ( insert only the single input layer )
16. input layer , fully connected layer to get output
17. there are available models -> we can choose which one we want to use and we need to ask ourself like which convolution should i do ,what I want to input , what filter we want to use?-> we pick the largest element
18. flatten -> to make them into single layer
19. dense -> layer 
20. flatten , dense , and optimization ( using the optimizer ) then there is MNIST in the code 
21. MNIST provide the code and error rate in wikipedia and there is like committee of numbers of CNNS ( Means how many times they use this CNNS )-> models that have been used and tested ( so it can be a choice for us to choose which one to try for our model) thats what I understood.
22. we can find the moist models ( benchmark. ai ,rodingob.github.io , wikipedia)


#2019-05-15#
1. today’s lecture was about automation and the future of work
2. lecturer began with a question whether we knew the third industrial revolution and i honestly don’t know what that is
3. the industrial evolution is divided into communicate & manage , power& fuel and move ( third of them are related to economic activity)
4. 3rd industrial evolution started with internet , smart world , renewable energies-> change the world and competition between countries
5. example : tesla -> autonomous car
6. Renewable energy in California -> the frequencies of the energy storage that we need to use to have a massive change
7. biosphere and social integrity -> like most of them are about environmental issues we are facing ( global warming , energy loss , acidicification
8. lecturers gave us a video and stats about the 60% of occupations that have >30% of their work related to automation and most of automation work that is currently being applied -> customer service is the highest
9. there are a stats written in tables told about the current technologies vs human performance which many current technologies are competing in human performance -> top one is at the navigation
10. and applied in south korea , japan, etc.
11. like norway it has a lowest risk of job potential -> since it has been automated like every small jobs since it has highest salary to given and one of the most expensive country to live -> lead them to locals don’t have any small jobs 
12. most of the countries are already applying automation work
13. in order to human face this automation -> we can control this AI -> automation need to be adapted by companies and automation works will lead
14. automation work expected to be conquer almost 100% , and yes it needs many people to work in making this automation work and people also need to grow in order to face it otherwise will lead to employment loss
15. USA is being challenged of the employment increase in rest of economoy except manufacturing and agriuculture that has been in a massive dcrease since about 200 years ago.
16. economic growth has changed a lot as industrial revolution has been happening
17. ai deep learning machine learning will continue on this automation work -> in this era is rapidly developing 
18. engineers of automation work wont lose its job 

#2019-05-22#
1. ethics and danger of algorithm bias 
2. when I saw the title , i recall my memories like how when I didn’t what AI really is and only know its some kind of robot , i thought what about this robot like in a movie , controlling the world and used in a war and made a bomb , etc.
3. in TED , if we want AI to evolve in humans we need to set goals 
4. professor asked us to discuss in pairs about the algorithm bias that is still failing and can be question its accuracy 
5. me and my partner discussed about COMPAS , an algorithm bias about recidivism ( potential the criminal to take a crime again) and will affect the sentencing judgment. first when we read about the purpose we thought oh ! its really good since it can reduce the human subjectiveness and as we read more it still lack of accuracy like discrimination between white and black defendant. so we still thought it still need to develop more since it can affect to a serious case and i thought it was kind of hard since it was reading like their personalities-> which I thought it would be hard to define
6. in the next TED video, she was talking that many AI robot can’t recognize her face and like what her AI she was working can’t recognize her since they use a same generic facial software and their facial recognitions’s algorithm bias hasn’t been inputed or trained of these kinds of face.
7. inclusive coding matters -< who codes matters , how we code matters and why we code matters.
8. we need to identifying more bias ( i thought like how faces differs + give more knowledges)and how we train them more.
9. third videos are about how a grad student made a recognize a fake person in videos/ photos.-> facial recognition using a 3d model and develop the texture from their online photos and they can modify the sounds just from their real audio of video-> synthesize texture
10. i think it is really dangerous about those fake sound since yes especially to the leader of the countries , it can lead to war and damage other people
11. i was wondering can we detect whether it was fake or not.

#2019-05-29# 
1. today we discussed about group projects 
2. i think its really good since it made me learn again about what lecturer has already taught us for this semester
3. actually i kind of find it hard since im not in a major that has ever learned these kind of things but im willing to learn and try my best

#2019-06-05#
1. today we only did discussion and we kind of know what kind of architecture we want to use but we ll try to get the best accuracy

#2019-06-12#
1. we saw some presentations and they did really good their accuracy is higher than my group and we need to do a lil bit more coding
<<<<<<< HEAD


=======
17. engineers developing automation work wont lose a job 
18. ai , deep learning , machine learning -> will lead in this era -> automation work
>>>>>>> origin/meghiya/meghi_chenmd-edited-online-with-bitbucke-1557892947995
>>>>>>> origin/Meghiya-Michelle/meghi_chenmd-edited-online-with-bitbucke-1560917033601

#2019-06-19#
1. today its the final day , it was kind of hard but i think its good and we had presentation
2. i was trying to do the emnist but due to time and it was fail
3. but from that , i learned something new which is using matplotlib in python and i found it really cool thanks
<<<<<<< HEAD

=======
>>>>>>> origin/Meghiya-Michelle/meghi_chenmd-edited-online-with-bitbucke-1560917033601
=======
13. but i think the hard thing that i still experienced is about my tensor flow knowledge like which one is better , what its function and how this code works that i think i haven’t understood it clearly but yes I’m now have a basic knowledge about it . thank you
>>>>>>> origin/meghiya/meghi_chenmd-edited-online-with-bitbucke-1556688924454
