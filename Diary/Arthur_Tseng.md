This diary file is written by Arthur Tseng E64056423 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20
* Worrying about this class whether it's gonna be too hard for me. But I still decided to take it because we should learn more about AI instead of avoiding it since AI might become more common in our generation.

# 2019-02-27
* The lecture was interesting, however I thought it might be more appealing if we talked about more actual cases while discussing the definitions. No offense, Just making an advice haha.
* I think that Ai is how we try to use modern technology to simulate the brain actions in order to take the advantage of both human thinking and machine calculating or working.
* Too bad that the time was not enough for us to do the presentation. I was really looking forward to other people's stories.

# 2019-03-06
* Still felt a bit confused about the diary, but I didn't even know the point during the class so I didn't know how to ask. But now I know it, I should ask more about how am I suppose to edit the file instead of making new branches. Anyway I'd solved the problem by myself haha.
* It feels the same while learning Python comparing with c++ and matlab, since it's all computer language. And it's probably more similar to matlab.
* I could fully understand the class. But I felt it might be a bit too fast for someone who hadn't learnt anything about programming yet.

# 2019-03-13
* An hour before today's lecture, I had problems opening the previous power point files. However after I went home the problem was solved. I guess it's all about the poor internet connection. The school should really improve it.
* The class was great, we got to practice and ask freely, it's very fun. Just so sad the class was too short, maybe next year the class should have more credits.
* During real practice, I realize that python is more easier than C++ but a bit harder than matlab, comparing with the two computer languages I had learnt.
* Still not sure the difference between the use of "help" and "?". I'll definately practice more later.

# 2019-03-20
* The video was so great, It clearly demonstrated how neural networks works. Made us much more easier to understand.
* 3 blue one brown, subscribed, haha.
* Supervised learning, we'll have to provide enough data and instruction to the mechanie, and it'll do the instructions by using the data we provide.
* Unsupervised learning, the data will be provided randomly, and the mechanie will sort out and conclude the results on it's own.
* Reinforced learning, we'll need a data simulator in order to provide enough data for the mechanie to let it explore by itself(ex. Alpha go)
* Supervised learning, similar to how humans learn from school. Unsupervised learning, similar to how we learn from our experiences. Reinforced learning, similar to the way we create things.

# 2019-03-27
* During today's class I found a bit hard to concentrate because I slept less last night. I'll try to get more sleep next time.
* In the beginning, I Thought the reason why there's no true model is because we can't count out all factors thats going to put an effect to the system. It's similar to the concept of dark substance.
* I wonder what would happen if we create the true model. Maybe we can create a new world?

# 2019-04-10
* Global maximum or minimum is not needed because it'll cost too much memory to store the datas which most of them will not be useful.
* I thought that by using the concept of gradient, try and error, and add the borders and the scales of the function we will be able to collect the datas of partial maximums or minimums.
* convolutional neural networks is a bit similar to puzzles.
* The concept of pooling layer reminds me of the experience while using photoshop, For example maximum pooling is like adding brightness to the bright part; average pooling is like adding contrast.

# 2019-04-17
* I thought that deep learning is hard because it could cause a lot efforts to build a layer and sorts of things. However, thanks to some of the people who had already built up these layer models, it's much easier to practice deep learning now.
* Each models can be only used on one purpose. Because it's only trained for it.
* Tensorflow is still upgrading. So we should keep learning to catch up with the newest technology.

# 2019-04-24
* Still working on trying to understand how to add layers in the exercise.
* I thought maybe next time we can break the coding part into smaller categories and explain the meaning of the code, after explaining the concept of deep learning.
* Even though I know how deep learning works, it still take a long time to figure out the coding part.

# 2019-05-01
* Today's class was wonderful, all of the questions I had since last week were all solved today.
* I found out a perfect website that can learn more about python, it's called "莫煩python".
* I still need a way to konw all of these code, like a dictionary, otherwise I can't code anything. As long as I have a dictionary of these code I can code everything.
* Each kind of layer will fit to different kind of situation, there's no best layer.

# 2019-05-08
* Thanks for answering my question. All problems were solved now.
* Reviewed the concepts of artificial neural network, convolution, and backprobagation.

# 2019-05-15
* I disagree with the idea of defining the forth industrial revolution as Ai production era. Because I thought Ai is also part of the automatically production which represents the third industrial revolution.
* It’s very important to keep our revolution going because there’s lot of environmental challenges today and if we don't improve we’re all doomed. 
* I disagree with the idea of teacher is not replaceable. Since the youtube had invented, teachers may be replaced by video one day.

# 2019-05-22
* One of the cases we discuess about algorithm bias today, was about MIT's psychological robot for criminal research use known as Norman. They feed it with violence and criminal data base, and it turn's out that the thought's out put from it than become very terrible.
* If some people uses some politic celebrities' faces to create a fake video of declaring wars for examples. It may come in huge problems.
* Men are still needed to make some huge decision because AIs can't be humanity after all.

# 2019-05-29
* So happy to met some new friends in class.
* Adding a relu layer will lower the accuracy.
* Changing the number of the first layer will also lower the accuracy.
* We'll try to find the best fit layer to the model, than modify the model later.

# 2019-06-05
* Today's class we discuessed about how to get a better accuracy.
* Convolutional layer is probably better while dealing with pictures.
* Mostly using the optimizer SGD will get better results comparing with Adam.
* Using both sigmoid and relu function is better than using one of them twice.

# 2019-06-12
* Today's class we show everybody our group work.
* I thought everyone is doing a great job on training models. We all have good results.
* Most surprising thing is that someone actually use the structure of alpha go, never thought of that before.
* I like everything in this class so far.

# 2019-06-19
* Hope that we have more time to finish the test.
* So glad that I took this class this semester, really learned a lot from it. Hope that I'll be able to apply it in my major studies later.
