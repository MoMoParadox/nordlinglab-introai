This diary file is written by Henry Chang E64076067 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #
* Today is the first day of the class. I met my senior.
* It's not surprised to see him here, he's a student with good grades.
* After class, actually I'm a little bit afraid of my future (this semester), because it seems that this class is too hard for me.
* I didn't get what the teacher said very well.
* But I decided to give myself a challenge.

# 2019-02-27 #
* Today is the second day of the class.
* Many things become better this week.
* I sat in the front of the classroom today so I can understand what the teacher was saying better.
* But I haven�t made any foreign friends ,still, which is the goal I gave myself last week.
* Next time I�m going to wait for some foreign classmates sitting down and I then go to sit next to them.
* In this way I can at least have some interactive with them.

# 2019-03-06 #
* The grammer of Phyton is a little bit strange. Even though I have studied C++ language before, I still find it hard to understand.
* I can still understand until "boolean and integar". After that, I have no idea why the computer print out things like that, but I'll make myself understand this week.
* I still can't change my diary to right structure. Keep finging ways!
* I knew another classmates today. She is nice.
* The reason why I didn't raise my hand when the tacher was asking who would help the REDAME better is because I think it's clear enough. I finish it very smoothly with its help. And I'll help make it clearer if I find anything not clearenough.

# 2019-03-06 #
* I get the sense of Python a little more.
* The playground ran so slowly that I wasted my time waitimg fo it.
* One hour is really too short for this class, everytime the tacher can't say many things before the class ends.

# 2019-03-20 #
* I can't finish the practice of python due to having no time.
* I'll finish it as soon as possible.
* My English is not good enough to understand the lecture, I have to memorize more words in this field.
* But I can understand the concept of different kinds of algorithm.

# 2019-03-27 #
* I forgot to type my diary on time again. How bad is it!
* I have to work harder to memorize all the vocabularies I need.
* I forgot the word "participation" so I did a dumb thing today.

# 2019-04-10 #
* I feel that I can't really follow the class, I really have to figure out some ways to keep.
* I finally remember my diary this week on time.
* I'm sorry that I have nothing to say to the class context because I didn't understand it.
* I'll edit my diary whenever I have new found.

# 2019-04-17 #
* I thought that we have to write the hidden function so it will be difficult.
* But things become easier if we only need to include the function which has already written.
* Every thing is out of my control. I'm wondering whether I can keep this class.