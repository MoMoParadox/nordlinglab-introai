This diary file is written by Martin Hodek P06088415 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-20 #

* Course had started, it is bit rough to get to know all the online stuff. I have never worked with bitbucked and moodle, partly in Chinese, is also quite puzzling.
* I have watched the intorduction video, read through Nordling Lab web and searched Bitbucket. Slightly overwhelmed. Homework comes next.
* While reading (and creating) AI sucessful stories (first homework), I have come to the conclusion, that if one can provide appropriate data set, on which AI can learn, it can then "quicky" perform various tasks over huge quantities of data, mainly conected to the data recognition. 
* AI can outperform humans in many fields, often due to higher computing capacity and sometimes due to greater "base knowledge", yet this should not be feared, but utilized.
* Still wondering, can AI get out of control, as it is depicted in Sci-fi? In my opinion no.

# Article summary #

* [Toward autonomous mapping and exploration for mobile robots through deep supervised learning](https://ieeexplore.ieee.org/abstract/document/8206050)
* Within the article, autonomous mapping and exploration of unknown environment utilizing supervised learning of DNN is tested and compared to information-theoretic optimization approach. (ITO)
* ITO approach is based on "computationally intensive real-time evaluation of a set of candidate sensing actions" to choose the best way for robot to explore the map and gain the highest amount of information.
* DNN utilization approach stands on "guessing" the best exploration path based on previously obtained knowledge (in supervised learning phase) of similar map types.
* As a conclusion, simple DNN method provides slightly worse results than Information-theoretic optimization approach, however requires less computational time/power.
* Even though DNN approach can be further improved to provide better results, ITO will be always better in terms of map exploration, yet with higher computational requirements. Pairing DNN and ITO approach can lead to interesting optimization results and may be topic for another study.